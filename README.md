![We are IAM Cloud](https://resources.iamcloud.net/images/GitLab/iac-markdown-header.gif)

[<img src="https://resources.iamcloud.net/images/GitLab/linkedin.png" height="40em" align="left" alt="Follow IAM Cloud on LinkedIn" title="Follow IAM Cloud on LinkedIn"/>](https://www.linkedin.com/company/iam-cloud-limited/) 
[<img src="https://resources.iamcloud.net/images/GitLab/twitter.svg" height="40em" align="left" alt="Follow IAM Cloud on Twitter" title="Follow IAM Cloud on Twitter"/>](https://www.twitter.com/iamcloud) 
[<img src="https://resources.iamcloud.net/images/GitLab/web.jpg" height="40em" align="left" alt="Visit www.iamcloud.com" title="Visit www.iamcloud.com"/>](https://www.iamcloud.com) 

<br/>
<br/>

# the description
---
This script we created for setting up a policy to enable or disable the ability for users to edit their Microsoft 365 Profile Photos.
There are also extra steps that are not included in the script that will need to be taken for this to be fully implemented, these are also outline in this guide.

To successfully prevent users from changing their own profile photos, you need to:
- Change your Outlook on the web policy.
- Set up SharePoint Online permissions.
- Block access to the direct photo settings link.


## the explanation
---

### outlook policy (via powershell)
The script will change the outlook policy and apply to all users.

Once these have been defined with your O365 Global admin credentials the Connect0365 function with use these to connect to Office365 and also to the MsolService 
The next function will use these creds to perform a �Get� for all users within your 365 environment and return these into the script. It will then show a count and display how many accounts there are to process.
Once it has a count it will go through each mailbox individually and apply the OwaMailboxPolicy that we set earlier
Once this has run through all users will either be restricted from setting O365 Profile Photo�s or be allowed to do so, depending on what was defined

### sharepoint online profile policy
1. Go to Microsoft 365 admin centre, in the left menu choose SharePoint

2. From the SharePoint Admin Centre click **More Features** and click **open** in the **User Profiles** section

<img src="https://resources.iamcloud.net/images/GitLab/Powershell/microsoft365-profile-photo-settings/SPO-more-features.png" alt="SPO More Features" title="SPO More Features"/>

3. Go to **Manage **User Properties**

<img src="https://resources.iamcloud.net/images/GitLab/Powershell/microsoft365-profile-photo-settings/SPO-user-profiles.png" alt="SPO User Profiles" title="SPO User Profiles"/>

4. Click **Picture** followed by **Edit Policy**

<img src="https://resources.iamcloud.net/images/GitLab/Powershell/microsoft365-profile-photo-settings/SPO-user-profiles-2.png" alt="SPO More Features" title="SPO More Features"/>

5. In the settings, remove the tick from **Allow users to edit values for this property** and clikc **Ok**

<img src="https://resources.iamcloud.net/images/GitLab/Powershell/microsoft365-profile-photo-settings/SPO-Picture-settings.png" alt="SPO More Features" title="SPO More Features"/>

### access to the url
A user who visits the https://outlook.office.com/mail/changephoto URL will be able to change their photo. It is not too easy to block this URL but can be done using firewall for internal networks however a more generic solution is to use Client Access Rules to completely block access to Outlook on the Web and its settings.
Users will still be able to use Outlook for their mailing needs.


## the usage
---
The script connects to Office 365 using the $username and $password variables, simply define these before running the script under the �Variable Definition� section at the very top of the script.

The first thing you need to do is to create a Mailbox Policy that we can use to enable or disable the setting we need to change. To do this we use the code:
```powershell
New-OwaMailboxPolicy -Name ("Policy Name") 
```
This can be found in the CreatePhotoPolicy Function, once this has been created you need to define whether you are enabling or disabling the picture policy by using one of the following commands:
```powershell
Set-OwaMailboxPolicy -SetPhotoEnabled $false
Set-OwaMailboxPolicy -SetPhotoEnabled $true 
```
$false disables the picture policy whilst $true enables the policy

Again, these are found in the SetPhotoPolicy, simply remove the starting # from the one you want to action then call the function.


## the licence
---
[MIT](http://opensource.org/licenses/MIT)


## the poeple
---
Ross Beedall,
Adam Cosby