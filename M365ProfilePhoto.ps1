################################
# Variable Definition
################################
$username = 
$password = 

################################
#FUNCTIONS
################################

Function ConnectM365($M365Username, $M365Password) {
    write-host "Authenticating to Office 365 as $M365Username..."
    $M365Password = ConvertTo-SecureString $M365Password -asplaintext -force
    $M365Cred = new-object -typename System.Management.Automation.PSCredential -argumentlist $M365Username, $M365Password
    Connect-MsolService -Credential $M365Cred
    if (!(Get-PSSession | Where { $_.ConfigurationName -eq "Microsoft.Exchange" }))
    {
        $Session = New-PSSession -ConfigurationName Microsoft.Exchange -ConnectionUri https://ps.outlook.com/powershell/ -Credential $M365Cred -Authentication Basic -AllowRedirection
        Import-PSSession $Session
    }
    write-host "Successfully connected to M365 and exchange online"
}

Function GetM365Users()
{
    # get mailboxes etc
    write-host "Getting Users from M365... this might take a few minutes"
    $Users = Get-Mailbox -resultsize unlimited
    return $Users
}

Function CreatePhotoPolicy
{
    New-OwaMailboxPolicy 
}
Function SetPhotoPolicy
{
    #Set-OwaMailboxPolicy -SetPhotoEnabled $false
    #Set-OwaMailboxPolicy -SetPhotoEnabled $true
}
################################
#Begin Script
################################

ConnectM365 $username $password

$Users = Users
if ($Users.Count -eq 0)
{
    Write-Host "There are no users to process"
} else {
    Write-Host "There are $($Users.Count) objects to process"

    Foreach ($User in $Users) {
        Set-CASMailbox -Identity $User -OwaMailboxPolicy "Profile Pictures Disable"
    }
}